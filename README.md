# Ghoststrap

A fully responsive Bootstrap theme based on the latest Bootstrap v4.0.0-beta.2
and Ghost 1.18.0.

The intent of this theme is to provide you the best starting point to create
your own style based on Bootstrap and Ghost.

## Demo
Please find a demo [here](http://ghoststrap.mostofghost.blog/).

## Screenshots
### Desktop
![Screenshot Desktop](https://bytebucket.org/couchsoft/ghoststrap/raw/master/assets/screenshot-desktop.png)

### Mobile
![Screenshot Mobile](https://bytebucket.org/couchsoft/ghoststrap/raw/master/assets/screenshot-mobile.png)

## Features
### Cookie notification
Ghoststrap Theme comes with a built in Cookie notification from https://cookieconsent.insites.com.
You don't need to configure anything - but if you want you can change the
default texts in `default.hbs`.

You can disable the notification by adding the following in your *Blog header*

```html
<script>
    HIDE_COOKIE_NOTIFICATION = true;
</script>
```

### Icons
The Theme uses Icons from http://ionicons.com (version 2.0.1). So can you: In your posts
just add for example `<i class="ion-forward"></i>`.

### Disqus comments
The theme supports Disqus comments. Simply add the following in your *Blog header*
(REPLACE with your ID!):

```html
<script>
    DISQUS_SITE_ID = 'MY_DISQUS_ID';
</script>
```

### Subscriptions
The theme supports subscriptions, just enable them in the Labs.

### German language
This theme comes with all static texts in german language. You can easily change
them by editing the `*.hbs` files.

## Adapt
You can easily create your own layout based on my Ghoststrap theme by just some
simple steps.

### Add a CSS file
Add your own css file in `assets/css/mystyle.css`.

### Import the file in default.hbs
Right after this line:

```html
<link rel="stylesheet" type="text/css" href="{{asset "css/screen.css"}}" />
```

Add a new line like this:
```html
<link rel="stylesheet" type="text/css" href="{{asset "css/mystyles.css"}}" />
```

### Overwrite bootstrap styles
Ghoststrap uses the default Bootstrap styles, simply override those to meet your needs!

Or use and import a Bootstrap template, e.g. from Bootswatch!

## Support
I will do my best to keep this theme up-to-date with Ghost and Bootstrap 4.

In case you still have any problems with the theme or any questions on how to change
the theme, please file a bug or proposal in [Bitbucket](https://bitbucket.org/couchsoft/ghoststrap/issues).

## License
Copyright (c) 2017 Simon Bernard - Released under the [MIT license](LICENSE).
